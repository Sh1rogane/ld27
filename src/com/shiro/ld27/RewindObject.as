package com.shiro.ld27
{
	import com.shiro.ld27.entities.LDEntity;

	public class RewindObject 
	{
		public var x:Number;
		public var y:Number;
		public var frameLabel:String;
		public var frame:int;
		public var flipped:Boolean;
		public var alpha:Number;
		
		public function RewindObject(origin:LDEntity, alpha:Number = 1) 
		{
			x = origin.x;
			y = origin.y;
			this.alpha = alpha;
			/*frameLabel = origin.sprMap.currentAnim;
			frame = origin.sprMap.frame;
			flipped = origin.sprMap.flipped;*/
		}
		
	}
}