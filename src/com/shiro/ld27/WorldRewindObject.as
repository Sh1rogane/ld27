package com.shiro.ld27
{
	import net.flashpunk.Entity;

	public class WorldRewindObject
	{
		public var e:Entity;
		public var time:Number;
		public function WorldRewindObject(e:Entity, time:Number)
		{
			this.e = e;
			this.time = time;
		}
	}
}