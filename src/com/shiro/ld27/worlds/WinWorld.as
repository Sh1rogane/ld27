package com.shiro.ld27.worlds
{
	import com.shiro.ld27.Assets;
	import com.shiro.ld27.Game;
	import com.shiro.ld27.entities.Time;
	
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.World;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	
	public class WinWorld extends World
	{
		private var winScreen:Image = new Image(Assets.WIN_SCREEN);
		private var ss:Entity = new Entity(0,0,winScreen);
		private var t:Time = new Time(100,150, "You won in: ", 50);
		public function WinWorld()
		{
			super();
			add(ss);
			add(t);
		}
		override public function update():void
		{
			super.update();
			t.text = "You won in: " + Game.times + " itterations!"
			if(Input.pressed(Key.M))
			{
				if(!Game.mute)
				{
					Game.mute = true;
					Assets.music.volume = 0;
				}
				else
				{
					Game.mute = false;
					Assets.music.volume = 0.7;
				}
				
			}
		}
	}
}