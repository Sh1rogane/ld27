package com.shiro.ld27.worlds
{
	import com.shiro.ld27.Assets;
	import com.shiro.ld27.Game;
	
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.World;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;

	public class StartWorld extends World
	{
		private var startScreen:Image = new Image(Assets.START_SCREEN);
		private var ss:Entity = new Entity(0,0,startScreen);
		public function StartWorld()
		{
			super();
			add(ss);
		}
		override public function update():void
		{
			super.update();
			if(Input.pressed(Key.M))
			{
				if(!Game.mute)
				{
					Game.mute = true;
					Assets.music.volume = 0;
				}
				else
				{
					Game.mute = false;
					Assets.music.volume = 0.7;
				}
				
			}
			if(Input.pressed(Key.SPACE))
			{
				FP.world = new PlayWorld();
			}
		}
	}
}