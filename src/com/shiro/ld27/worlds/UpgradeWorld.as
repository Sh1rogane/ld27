package com.shiro.ld27.worlds
{
	import com.shiro.ld27.Assets;
	import com.shiro.ld27.Game;
	import com.shiro.ld27.entities.Button;
	import com.shiro.ld27.entities.Time;
	
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.World;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	
	public class UpgradeWorld extends World
	{
		private var startScreen:Image = new Image(Assets.UPGRADE_SCREEN);
		private var ss:Entity = new Entity(0,0,startScreen);
		
		private var fireSpeedBtn:Button = new Button(128,150,Assets.FIRESPEED_BTN);
		private var damageBtn:Button = new Button(128,290,Assets.DAMAGE_BTN);
		private var timeBtn:Button = new Button(128,430,Assets.TIME_BTN);
		private var bulletBtn:Button = new Button(510,150,Assets.BULLET_BTN);
		private var missileBtn:Button = new Button(510,290,Assets.MISSILE_BTN);
		private var botBtn:Button = new Button(510,430,Assets.BOT_BTN);
		
		private var text1:Time = new Time(238,150,"cost", 24);
		private var text2:Time = new Time(238,290,"cost", 24);
		private var text3:Time = new Time(238,430,"cost", 24);
		private var text4:Time = new Time(620,150,"cost", 24);
		private var text5:Time = new Time(620,290,"cost", 24);
		private var text6:Time = new Time(620,430,"cost", 24);
		
		private var text11:Time = new Time(238,200,"lvl", 24);
		private var text22:Time = new Time(238,340,"lvl", 24);
		private var text33:Time = new Time(238,480,"lvl", 24);
		private var text44:Time = new Time(620,200,"lvl", 24);
		private var text55:Time = new Time(620,340,"lvl", 24);
		private var text66:Time = new Time(620,480,"lvl", 24);
		
		private var money:Time = new Time(50,20,"Money: ", 32);
		
		private var playBtn:Button = new Button(400, 550, Assets.PLAY_BTN);
		
		public function UpgradeWorld()
		{
			super();
			Game.rewind = false;
			
			add(ss);
			add(fireSpeedBtn);
			add(damageBtn);
			add(timeBtn);
			add(bulletBtn);
			add(missileBtn);
			add(botBtn);
			
			add(text1);
			add(text2);
			add(text3);
			add(text4);
			add(text5);
			add(text6);
			
			add(text11);
			add(text22);
			add(text33);
			add(text44);
			add(text55);
			add(text66);
			
			add(money)
			
			add(playBtn);
		}
		override public function update():void
		{
			super.update();
			
			if(Input.pressed(Key.M))
			{
				if(!Game.mute)
				{
					Game.mute = true;
					Assets.music.volume = 0;
				}
				else
				{
					Game.mute = false;
					Assets.music.volume = 0.7;
				}
				
			}
			var cost1:Number = Game.fireSpeedLvl * 100;
			var cost2:Number = Game.damageLvl * 100;
			var cost3:Number = (1 + Game.timeLvl) * 500;
			var cost4:Number = Game.bulletLvl * 100;
			var cost5:Number = (1 + Game.missileLvl) * 200;
			var cost6:Number = (1 + Game.botLvl) * 200;
			
			text1.text = "cost: " + cost1;
			text2.text = "cost: " + cost2;
			text3.text = "cost: " + cost3;
			text4.text = "cost: " + cost4;
			text5.text = "cost: " + cost5;
			text6.text = "cost: " + cost6;
			
			text11.text = "Lvl: " + Game.fireSpeedLvl;
			text22.text = "Lvl: " + Game.damageLvl;
			text33.text = "Lvl: " + Game.timeLvl;
			text44.text = "Lvl: " + Game.bulletLvl;
			text55.text = "Lvl: " + Game.missileLvl;
			text66.text = "Lvl: " + Game.botLvl;
			
			money.text = "Money: " + Game.money.toFixed(0);
			
			if(Game.fireSpeedLvl == 5)
			{
				text1.text = "cost: MAX";
				text11.text = "Lvl: MAX";
			}
			if(Game.damageLvl == 5)
			{
				text2.text = "cost: MAX";
				text22.text = "Lvl: MAX";
			}
			if(Game.timeLvl == 2)
			{
				text3.text = "cost: MAX";
				text33.text = "Lvl: MAX";
			}
			if(Game.bulletLvl == 3)
			{
				text4.text = "cost: MAX";
				text44.text = "Lvl: MAX";
			}
			if(Game.missileLvl == 3)
			{
				text5.text = "cost: MAX";
				text55.text = "Lvl: MAX";
			}
			if(Game.botLvl == 3)
			{
				text6.text = "cost: MAX";
				text66.text = "Lvl: MAX";
			}
			
			if(fireSpeedBtn.clicked && Game.fireSpeedLvl < 5 && Game.money >= cost1)
			{
				Game.fireSpeedLvl++;
				Game.money -= cost1;
			}
			else if(damageBtn.clicked && Game.damageLvl < 5 && Game.money >= cost2)
			{
				Game.damageLvl++;
				Game.money -= cost2;
			}
			else if(timeBtn.clicked && Game.timeLvl < 2 && Game.money >= cost3)
			{
				Game.timeLvl++;
				Game.money -= cost3;
			}
			else if(bulletBtn.clicked && Game.bulletLvl < 3 && Game.money >= cost4)
			{
				Game.bulletLvl++;
				Game.money -= cost4;
			}
			else if(missileBtn.clicked && Game.missileLvl < 3 && Game.money >= cost5)
			{
				Game.missileLvl++;
				Game.money -= cost5;
			}
			else if(botBtn.clicked && Game.botLvl < 3 && Game.money >= cost6)
			{
				Game.botLvl++;
				Game.money -= cost6;
			}
			else if(playBtn.clicked)
			{
				FP.world = new PlayWorld();
				Input.mouseCursor = "auto";
			}
		}
	}
}