package com.shiro.ld27.worlds
{
	import com.shiro.ld27.Assets;
	import com.shiro.ld27.Game;
	import com.shiro.ld27.WorldRewindObject;
	import com.shiro.ld27.entities.Background;
	import com.shiro.ld27.entities.Boss;
	import com.shiro.ld27.entities.Bot;
	import com.shiro.ld27.entities.Button;
	import com.shiro.ld27.entities.Dialog;
	import com.shiro.ld27.entities.Dialog2;
	import com.shiro.ld27.entities.Explosion;
	import com.shiro.ld27.entities.Player;
	import com.shiro.ld27.entities.Time;
	
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.World;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.utils.Draw;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	
	import punk.blur.BlurCanvas;
	
	public class PlayWorld extends World
	{
		private var rewindV:Vector.<WorldRewindObject> = new Vector.<WorldRewindObject>();
		private var addV:Vector.<WorldRewindObject> = new Vector.<WorldRewindObject>();
		private var firstW:WorldRewindObject;
		private var lastW:WorldRewindObject;
		
		private var time:Number = 10;
		private var timeText:Time = new Time(380, 50, "10.00");
		private var scoreText:Time = new Time(700,50, "0");
		private var d:Dialog = new Dialog(100,350);
		private var d2:Dialog2 = new Dialog2(100,350);
		
		private var player:Player;
		private var boss:Boss;
		
		private var start:Boolean = true;
		
		private var t:Number = 0;
		
		private var e:Explosion;

		public function PlayWorld()
		{
			super();
			scoreText.text = "" + Game.money.toFixed(0);
			
			Game.blurCanvas = new BlurCanvas(0.98);
			
			if(!Assets.music.playing)
				Assets.music.loop(0.7);
			
			player = new Player(400,700);
			boss = new Boss(200, -200);
			add(new Background());
			add(Game.blurCanvas);
			add(timeText);
			add(scoreText);
			add(player);
			add(boss);
			if(Game.botLvl > 0)
			{
				add(new Bot(player.x,player.y,player,1));
				if(Game.botLvl > 1)
				{
					add(new Bot(player.x, player.y, player, -1));
				}
			}
			Game.times++;
		}
		override public function update():void
		{
			if(!Game.pause && !start)
			{
				super.update();
				if(Input.pressed(Key.M))
				{
					if(!Game.mute)
					{
						Game.mute = true;
						Assets.music.volume = 0;
					}
					else
					{
						Game.mute = false;
						Assets.music.volume = 0.7;
					}
						
				}
				if(!Game.rewind)
				{
					time -= FP.elapsed / (1 + Game.timeLvl);
					Game.money += 50  * FP.elapsed
					if(time <= 0)
					{
						time = 0;
						Game.pause = true;
						add(d);
					}
					if(player.dead)
					{
						Game.pause = true;
						add(d2);
					}
					if(boss.hp <= 0)
					{
						Game.pause = true;
						e = new Explosion(boss.x + 200, boss.y + 50);
						add(e);
					}
					scoreText.text = Game.money.toFixed(0);
				}
				else
				{
					do
					{
						if(lastW != null)
						{
							if(lastW.time <= time + 0.05 && lastW.time >= time - 0.05)
							{
								add(lastW.e);
								lastW = null;
							}
							else
							{
								break;
							}
						}
						if(lastW == null)
						{
							lastW = rewindV.pop();
						}
					}while(lastW != null);
					do
					{
						if(firstW != null)
						{
							if(firstW.time <= time + 0.05 && firstW.time >= time - 0.05)
							{
								remove(firstW.e);
								firstW = null;
							}
							else
							{
								break;
							}
						}
						if(firstW == null)
						{
							firstW = addV.pop();
						}
					}while(firstW != null);
					time += FP.elapsed;
					if(Input.pressed(Key.SPACE))
						FP.world = new UpgradeWorld();
				}
				if(time < 10)
					timeText.text = "" + time.toFixed(2);
				else
					timeText.text = "10.00";
				
				if((boss.y < -100 && player.y > 600))
					FP.world = new UpgradeWorld();
			}
			else if(Game.pause)
			{
				if(boss.hp <= 0)
				{
					e.update();
					if(e.t > 3)
						FP.world = new WinWorld();
				}
				else
				{
					if(Input.pressed(Key.SPACE))
					{
						if(player.dead)
							remove(d2);
						else
							remove(d);
						
						Game.pause = false;
						Game.rewind = true;
					}
				}
				
			}
			else if(start)
			{
				super.update();
				if(!player.start && !boss.start)
					start = false;
			}
		}
		override public function remove(e:Entity):Entity
		{
			var ee:Entity = super.remove(e);
			if(!Game.rewind && !Game.pause && !start)
				rewindV.push(new WorldRewindObject(ee,time));
			return ee;
		}
		override public function add(e:Entity):Entity
		{
			var ee:Entity = super.add(e);
			if(!Game.rewind && !Game.pause && !start)
				addV.push(new WorldRewindObject(ee,time));
			return ee;
		}
	}
}