package com.shiro.ld27
{
	import net.flashpunk.Sfx;

	public class Assets
	{
		[Embed(source = 'assets/Music.mp3')] 
		public static const MUSIC:Class;
		
		[Embed(source = 'assets/StartScreen.png')] 
		public static const START_SCREEN:Class;
		
		[Embed(source = 'assets/UppgradeScreen.png')] 
		public static const UPGRADE_SCREEN:Class;
		
		[Embed(source = 'assets/WinScreen.png')] 
		public static const WIN_SCREEN:Class;
		
		[Embed(source = 'assets/FireSpeedBtn.png')] 
		public static const FIRESPEED_BTN:Class;
		[Embed(source = 'assets/DamageBtn.png')] 
		public static const DAMAGE_BTN:Class;
		[Embed(source = 'assets/TimeBtn.png')] 
		public static const TIME_BTN:Class;
		[Embed(source = 'assets/BulletBtn.png')] 
		public static const BULLET_BTN:Class;
		[Embed(source = 'assets/MissileBtn.png')] 
		public static const MISSILE_BTN:Class;
		[Embed(source = 'assets/BotBtn.png')] 
		public static const BOT_BTN:Class;
		[Embed(source = 'assets/PlayBtn.png')] 
		public static const PLAY_BTN:Class;
		
		[Embed(source = 'assets/Block.png')] 
		public static const BLOCK:Class;
		
		[Embed(source = 'assets/Bullet.png')] 
		public static const BULLET:Class;
		[Embed(source = 'assets/BossBullet1.png')] 
		public static const BOSS_BULLET1:Class;
		[Embed(source = 'assets/BossBullet2.png')] 
		public static const BOSS_BULLET2:Class;
		[Embed(source = 'assets/BossBullet3.png')] 
		public static const BOSS_BULLET3:Class;
		
		[Embed(source = 'assets/Missile.png')] 
		public static const MISSILE:Class;
		
		[Embed(source = 'assets/Laser.png')] 
		public static const LASER:Class;
		
		[Embed(source = 'assets/Player.png')] 
		public static const PLAYER:Class;
		
		[Embed(source = 'assets/Boss.png')] 
		public static const BOSS:Class;
		
		[Embed(source = 'assets/Bot.png')] 
		public static const BOT:Class;
		
		[Embed(source = 'assets/Dialog.png')] 
		public static const DIALOG:Class;
		[Embed(source = 'assets/Dialog2.png')] 
		public static const DIALOG2:Class;
		
		[Embed(source = 'assets/bulletSound.mp3')]
		public static const BULLET_SOUND:Class;
		
		[Embed(source = 'assets/MissileSound.mp3')]
		public static const MISSILE_SOUND:Class;
		
		public static var bulletSound:Sfx = new Sfx(BULLET_SOUND);
		public static var missileSound:Sfx = new Sfx(MISSILE_SOUND);
		public static var music:Sfx = new Sfx(MUSIC);
	}
}