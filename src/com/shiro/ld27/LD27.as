package com.shiro.ld27
{
	import com.shiro.ld27.worlds.PlayWorld;
	import com.shiro.ld27.worlds.StartWorld;
	import com.shiro.ld27.worlds.UpgradeWorld;
	
	import net.flashpunk.Engine;
	import net.flashpunk.FP;
	
	[SWF(width='800',height='600',backgroundColor='#ffffff',frameRate='60')]
	[Frame(factoryClass="com.shiro.ld27.Preloader")]
	public class LD27 extends Engine
	{
		public function LD27()
		{
			super(800,600);
			//FP.console.enable();
		}
		
		override public function init():void
		{
			trace("FlashPunk has started successfully!");
			//Start StartSceen
			//FP.world = new UpgradeWorld();
			FP.world = new StartWorld();
		}
	}
}