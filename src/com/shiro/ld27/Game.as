package com.shiro.ld27
{
	import punk.blur.BlurCanvas;

	public class Game
	{
		public static var blurCanvas:BlurCanvas = new BlurCanvas(0.90);
		
		public static var times:Number = 0;
		
		public static var mute:Boolean;
		
		public static var pause:Boolean;
		public static var rewind:Boolean;
		
		public static var firstTime:Boolean = true;
		
		public static var timerSpeed:Number = 1;
		
		public static var money:Number = 0;
		
		public static var fireSpeed:Number = 0.5;
		public static var fireSpeedLvl:Number = 1;
		
		public static var damageLvl:Number = 1;
		
		public static var timeLvl:Number = 0;
		public static var bulletLvl:Number = 1;
		public static var missileLvl:Number = 0;
		public static var botLvl:Number = 0;
		
		
	}
}