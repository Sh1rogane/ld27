package com.shiro.ld27
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public class Preloader extends MovieClip 
	{
		public var mainClass:Class = com.shiro.ld27.LD27;
		public var bounds:Rectangle = new Rectangle(0, 0, 800, 600);
		
		public var minTime:int = 0;
		
		[Embed(source = "../assets/100x100strip.png")] 
		private var banner:Class;
		
		private var count:int = 0;
		private var t:int = 0;
		
		public var buffer:BitmapData;
		private var background:Bitmap;
		private var addedComplete:Boolean = false;
		
		public function Preloader() 
		{
			addEventListener(Event.ADDED_TO_STAGE, start); 
		}
		private function start(e:Event):void
		{
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			
			addEventListener(Event.ENTER_FRAME, onFrame);
			loaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ioError);
			
			buffer = new BitmapData(bounds.width / 2, bounds.height / 2, false, 0xFF202020);
			background = new Bitmap(buffer);
			background.scaleX = background.scaleY = 2;
			addChild(background);
		}
		public function onFrame(e:Event):void
		{
			if ((t % 4) == 0)
			{
				buffer.copyPixels((new banner).bitmapData, new Rectangle(count * 100, 0, 100, 100), new Point((bounds.width / 4) - 50, (bounds.height / 4) -50));
				(count < 4) ? count++: count = 0;
			}
			buffer.fillRect(new Rectangle(0, (bounds.height / 2) - 25, ((loaderInfo.bytesLoaded / loaderInfo.bytesTotal) * (bounds.width / 2)), 25), 0xFFFF3366);
			t++;
			if (t >= minTime)
			{
				if ((loaderInfo.bytesLoaded / loaderInfo.bytesTotal) == 1)
					loadingFinished();
			}
		}
		private function ioError(e:IOErrorEvent):void 
		{ 
			trace(e.text); 
		}
		public function loadingFinished():void 
		{
			loaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, ioError);
			removeEventListener(Event.ENTER_FRAME, onFrame);
			removeChild(background);
			
			var m:DisplayObject = new mainClass() as DisplayObject;
			addChild(m);
		}
	}
}