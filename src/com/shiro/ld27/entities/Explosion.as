package com.shiro.ld27.entities
{
	import com.shiro.ld27.Assets;
	
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Image;
	
	public class Explosion extends Entity
	{
		private var g:Image = new Image(Assets.BLOCK);
		public var t:Number = 0;
		public function Explosion(x:Number=0, y:Number=0)
		{
			super(x, y, g, null);
			g.centerOrigin();
		}
		override public function update():void
		{
			super.update();
			g.scale += 30 * FP.elapsed;
			t += FP.elapsed;
		}
	}
}