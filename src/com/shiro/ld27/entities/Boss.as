package com.shiro.ld27.entities
{
	import com.shiro.ld27.Assets;
	import com.shiro.ld27.Game;
	
	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	
	import net.flashpunk.FP;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.utils.Draw;
	
	public class Boss extends LDEntity
	{
		private var g:Image = new Image(Assets.BOSS);
		private var hpText:Text = new Text("1000", 0, -20);
		
		public var hp:Number = 1000;
		
		private var vx:Number = 1;
		private var speed:Number = 50;
		
		private var weakSpot1:Rectangle = new Rectangle(x,y,40,50);
		private var weakSpot2:Rectangle = new Rectangle(x,y,40,50);
		
		public var start:Boolean = true;
		
		private var fireType1:Number = -1;
		private var fireType2:Number = -1;
		private var fireType3:Number = -1;
		
		
		
		public function Boss(x:Number=0, y:Number=0)
		{
			super(x, y, g);
			this.setHitbox(g.width,g.height);
			this.addGraphic(hpText);
			hpText.size = 20;
		}
		override public function update():void
		{
			super.update();
			if(!start)
			{
				weakSpot1.x = x + 15;
				weakSpot1.y = y + 50;
				
				weakSpot2.x = x + width - 50;
				weakSpot2.y = y + 50;
				
				if(!Game.rewind)
				{
					checkBullet();
					fire();
				}
				if(this.x + this.width + vx * speed * FP.elapsed > FP.screen.width)
				{
					vx = -1;
				}
				else if(this.x + vx * speed * FP.elapsed < 0)
				{
					vx = 1;
				}
				x += vx * speed * FP.elapsed;
				hpText.text = "" + hp;
			}
			if(start)
			{
				this.y += 50 * FP.elapsed;
				if(this.y > 100)
					start = false;
			}
		}
		private function fire():void
		{
			fireType1 += FP.elapsed;
			fireType2 += FP.elapsed;
			fireType3 += FP.elapsed;
			
			if(fireType1 > 1)
			{
				FP.world.add(new BossBullet(weakSpot1.x + 20,200,1));
				FP.world.add(new BossBullet(weakSpot2.x + 20,200,1));
				fireType1 = 0;
			}
			if(fireType2 > 2)
			{
				FP.world.add(new BossBullet2(x + 175,y + 90,1));
				fireType2 = 0;
			}
			if(fireType3 > 3)
			{
				fireType3 = 0;
			}
		}
		private function checkBullet():void
		{
			var b:Bullet = collide("bullet",x,y) as Bullet;
			if(b)
			{
				var r:Rectangle = new Rectangle(b.x,b.y,b.width,b.height);
				//Do something
				if(weakSpot1.intersects(r) || weakSpot2.intersects(r))
				{
					Game.money += b.damage * 2;
					hp -= b.damage * 2;
					b.destroy(b.damage * 2);
				}
				else
				{
					Game.money += b.damage;
					hp -= b.damage;
					b.destroy(b.damage);
				}
				
				
			}
		}
	}
}