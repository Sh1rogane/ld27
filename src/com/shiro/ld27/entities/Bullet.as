package com.shiro.ld27.entities
{
	import com.shiro.ld27.Assets;
	import com.shiro.ld27.Game;
	
	import net.flashpunk.FP;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	
	public class Bullet extends LDEntity
	{
		protected var speed:Number = 500;
		protected var dir:int;
		public var damage:Number = 1;
		
		public function Bullet(x:Number=0, y:Number=0, direction:int = -1, graphics:Graphic = null)
		{
			super(x, y, graphics);
			this.dir = direction;
			this.type = "bullet";
			this.setHitbox(5,15);
		}
		override public function update():void
		{
			super.update();
			/*if(!Game.rewind)
			{
				this.y += speed * FP.elapsed * dir;
				if(this.y > FP.screen.height + 100 || this.y < -100)
				{
					destroy(0);
				}
			}*/
		}
		override public function destroy(damage:Number = 0):void
		{
			createText(damage);
			super.destroy();
		}
		private function createText(damage:Number):void
		{
			FP.world.add(new FlyingText(x,y,damage));
		}
	}
}