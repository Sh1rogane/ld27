package com.shiro.ld27.entities
{
	import com.shiro.ld27.Game;
	import com.shiro.ld27.RewindObject;
	import com.shiro.ld27.worlds.PlayWorld;
	
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	
	import punk.blur.BlurredGraphic;
	
	public class LDEntity extends Entity
	{
		protected var rewindArray:Vector.<RewindObject> = new Vector.<RewindObject>();
		
		protected var blurG:BlurredGraphic;
		
		protected var a:Number = 1;
		
		public function LDEntity(x:Number=0, y:Number=0, graphic:Graphic = null)
		{
			super(x, y, null, null);
			blurG = new BlurredGraphic(graphic, Game.blurCanvas);
			this.graphic = blurG;
		}
		override public function update():void
		{
			blurG.isBlurring = false;
			if(Game.rewind)
			{
				rewind();
				return;
			}
			else
			{
				rewindArray.push(new RewindObject(this, a));
			}
		}
		public function rewind():void
		{
			blurG.isBlurring = true;
			//rewindArray.pop();
			for(var i:int = 0; i < Game.timeLvl; i++)
			{
				rewindArray.pop();
			}
			var ro:RewindObject = rewindArray.pop();
			if(ro != null)
			{
				this.x = ro.x;
				this.y = ro.y;
				this.a = ro.alpha;
				//this.sprMap.play(ro.frameLabel, false, ro.frame);
				//this.sprMap.flipped = ro.flipped;
			}
		}
		public function destroy(damage:Number = 0):void
		{
			FP.world.remove(this);
		}
	}
}