package com.shiro.ld27.entities
{
	import flash.display.BitmapData;
	
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.utils.Draw;
	
	public class Background extends Entity
	{
		private var stars:Array = new Array();
		public function Background()
		{
			super(0, 0, null, null);
			var bd:BitmapData = new BitmapData(5,5,true,0xFFFFFF);
			Draw.setTarget(bd);
			Draw.circlePlus(0,0,2);
			for(var i:int = 0; i < 100; i++)
			{
				var star:Stamp = new Stamp(bd, Math.random() * FP.screen.width, Math.random() * FP.screen.height);
				stars.push(star);
				addGraphic(star);
			}
		}
		override public function update():void
		{
			for(var i:int = 0; i < stars.length; i++)
			{
				stars[i].y += 100 * FP.elapsed;
				if(stars[i].y > FP.screen.height)
				{
					stars[i].y = -10;
				}
			}
		}
	}
}