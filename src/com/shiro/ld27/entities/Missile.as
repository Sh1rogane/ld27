package com.shiro.ld27.entities
{
	import com.shiro.ld27.Assets;
	import com.shiro.ld27.Game;
	
	import net.flashpunk.FP;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	
	public class Missile extends Bullet
	{
		private var g:Graphic = new Image(Assets.MISSILE);
		
		public function Missile(x:Number=0, y:Number=0, direction:int = -1)
		{
			super(x, y, direction, g);
			this.setHitbox(10,20);
			this.damage = 4 * Game.damageLvl;
			if(Game.missileLvl == 3)
				this.damage += 5;
			
			Assets.missileSound.play(0.5);
		}
		override public function update():void
		{
			super.update();
			
			if(!Game.rewind)
			{
				this.y += speed * FP.elapsed * dir;
				if(this.y > FP.screen.height + 100 || this.y < -100)
				{
					destroy(0);
				}
			}
		}
	}
}