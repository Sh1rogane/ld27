package com.shiro.ld27.entities
{
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Text;
	
	public class Time extends Entity
	{
		private var timeText:Text = new Text("", 0, 0);
		
		public function Time(x:Number=0, y:Number=0, text:String = "", size:Number = 32)
		{
			super(x, y, timeText, null);
			timeText.size = size;
			timeText.text = text;
		}
		
		public function set text(text:String):void
		{
			timeText.text = text;
		}
	}
}