package com.shiro.ld27.entities
{
	import com.shiro.ld27.Assets;
	import com.shiro.ld27.Game;
	
	import net.flashpunk.FP;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	
	public class Bot extends LDEntity
	{
		private var g:Image = new Image(Assets.BOT);
		private var p:Player;
		private var side:int;
		
		private var fireTime:Number = 1;
		
		public function Bot(x:Number=0, y:Number=0, player:Player = null, side:int = 1)
		{
			super(x, y, g);
			p = player;
			this.side = side;
		}
		override public function update():void
		{
			super.update();
			if(!Game.rewind && !Game.pause)
			{
				fireTime += FP.elapsed;
				if(side == 1)
					this.x = p.x + (side * 100);
				else
					this.x = p.x + (side * 68);
				this.y = p.y + 20;
				if(fireTime > 0.3 && !p.start)
				{
					FP.world.add(new LaserBullet(x + 11, y - 3));
					fireTime = 0;
				}
					
			}
		}
	}
}