package com.shiro.ld27.entities
{
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.utils.Input;
	
	public class Button extends Entity
	{
		private var g:Image;
		public var clicked:Boolean;
		private var ins:Boolean;
		
		public function Button(x:Number=0, y:Number=0, buttonGraphic:Class=null)
		{
			g = new Image(buttonGraphic);
			super(x, y, g, null);
			this.setHitbox(g.width, g.height);
		}
		override public function update():void
		{
			super.update();
			clicked = false;
			if(inside(Input.mouseX,Input.mouseY))
			{
				ins = true;
				Input.mouseCursor = "button";
				if(Input.mousePressed)
				{
					clicked = true;
				}
			}
			else
			{
				if(ins)
				{
					ins = false;
					Input.mouseCursor = "auto";
				}
			}
			
		}
		private function inside(mX:Number, mY:Number):Boolean
		{
			return this.collidePoint(x,y,mX,mY);
		}
	}
}