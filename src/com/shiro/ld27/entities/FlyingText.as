package com.shiro.ld27.entities
{
	import com.shiro.ld27.Game;
	import com.shiro.ld27.RewindObject;
	
	import net.flashpunk.FP;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Text;
	
	public class FlyingText extends LDEntity
	{
		private var text:Text = new Text("1");
		private var vx:Number;
		private var vy:Number;
		
		public function FlyingText(x:Number=0, y:Number=0, text:Number = 0)
		{
			super(x, y, this.text);
			vx = Math.random() * 200 - 100;
			vy = Math.random() * -100 - 200;
			this.text.color = 0xFF0000;
			this.text.size = 10 + (2 * text);
			this.text.text = "" + text;
			this.x -= this.text.size / 2;
			this.y -= this.text.size / 2;
		}
		override public function update():void
		{
			super.update();
			if(!Game.rewind)
			{
				vy += 10;
				
				this.x += vx * FP.elapsed;
				this.y += vy * FP.elapsed;
				this.a -= 1 * FP.elapsed;
				text.alpha = a;
				if(text.alpha <= 0)
				{
					destroy();
				}
			}
			text.alpha = a;
		}
	}
}