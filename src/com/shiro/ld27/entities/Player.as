package com.shiro.ld27.entities
{
	import com.shiro.ld27.Assets;
	import com.shiro.ld27.Game;
	
	import net.flashpunk.FP;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;

	public class Player extends LDEntity
	{
		private var speed:Number = 30;
		private var g:Image = new Image(Assets.PLAYER);
		
		private var vx:Number = 0;
		private var vy:Number = 0;
		private var friction:Number = 0.95;
		private var maxSpeed:Number = 200;
		
		private var fireTime:Number = 1;
		private var missileTime:Number = 1;
		
		public var start:Boolean = true;
		public var dead:Boolean = false;

		public function Player(x:Number, y:Number)
		{
			super(x,y,g); 
			this.setHitbox(60, 20, 2, -30);
			
		}
		override public function update():void
		{
			super.update();
			if(!Game.rewind && !start)
			{
				if(Input.check(Key.RIGHT))
					vx += speed;
				else if(Input.check(Key.LEFT))
					vx += speed * -1;
				else
					vx *= friction;
				
				if(Input.check(Key.UP))
					vy += speed * -1;
				else if(Input.check(Key.DOWN))
					vy += speed;
				else
					vy *= friction;
				
				fire();
				
				if(vx > maxSpeed)
					vx = maxSpeed;
				else if(vx < -maxSpeed)
					vx = -maxSpeed
				if(vy > maxSpeed)
					vy = maxSpeed;
				else if(vy < -maxSpeed)
					vy = -maxSpeed;
						
				if(this.x + vx * FP.elapsed + width >= FP.screen.width || this.x + vx * FP.elapsed <= 0)
					vx = 0;
				if(this.y + vy * FP.elapsed + height >= FP.screen.height || this.y + vy * FP.elapsed <= 0)
					vy = 0;
				
				var b:Bullet = this.collide("enemyBullet",x,y) as Bullet;
				if(b)
				{
					dead = true;
				}
				
				this.x += vx * FP.elapsed;
				this.y += vy * FP.elapsed;
			}
			if(start)
			{
				this.y -= 30 * FP.elapsed;
				if(this.y < 500)
					start = false;
			}
		}
		private function fire():void
		{
			fireTime += FP.elapsed * (1 + Game.fireSpeedLvl / 5);
			missileTime += FP.elapsed * (1 + Game.fireSpeedLvl / 5);
			if(Input.check(Key.SPACE))
			{
				if(missileTime >= Game.fireSpeed * 2)
				{
					if(Game.missileLvl > 0)
					{
						if(Game.missileLvl > 2)
						{
							FP.world.add(new Missile(x + 47,y + 20,-1));
							FP.world.add(new Missile(x + 7,y + 20,-1));
						}
						else
						{
							FP.world.add(new Missile(x + 47,y + 20,-1));
						}
					}
					missileTime = 0;
				}
				if(fireTime >= Game.fireSpeed)
				{
					FP.world.add(new BasicBullet(x + 27,y - 10,-1));
					fireTime = 0;
					for(var i:int = 0; i < Game.bulletLvl - 1; i++)
					{
						FP.world.add(new BasicBullet(x + 17 + (20 * i),y - 15, -1));
					}
				}
			}
			
		}
	}
}