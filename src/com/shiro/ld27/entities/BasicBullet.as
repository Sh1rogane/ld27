package com.shiro.ld27.entities
{
	import com.shiro.ld27.Assets;
	import com.shiro.ld27.Game;
	
	import net.flashpunk.FP;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	
	public class BasicBullet extends Bullet
	{
		private var g:Graphic = new Image(Assets.BULLET);
		
		public function BasicBullet(x:Number=0, y:Number=0, direction:int = -1)
		{
			super(x, y, direction, g);
			this.type = "bullet";
			this.setHitbox(10,20);
			this.damage = Game.damageLvl;
			Assets.bulletSound.play();
		}
		override public function update():void
		{
			super.update();
			
			if(!Game.rewind)
			{
				this.y += speed * FP.elapsed* dir;
				if(this.y > FP.screen.height + 100 || this.y < -100)
				{
					destroy(0);
				}
			}
		}
	}
}