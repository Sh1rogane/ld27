package com.shiro.ld27.entities
{
	import com.shiro.ld27.Assets;
	
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Image;
	
	public class Dialog extends Entity
	{
		private var g:Image = new Image(Assets.DIALOG);
		public function Dialog(x:Number=0, y:Number=0, graphic:Graphic=null, mask:Mask=null)
		{
			super(x, y, g, null);
		}
	}
}