package com.shiro.ld27.entities
{
	import com.shiro.ld27.Assets;
	
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Image;
	
	public class Dialog2 extends Entity
	{
		private var g:Image = new Image(Assets.DIALOG2);
		public function Dialog2(x:Number=0, y:Number=0, graphic:Graphic=null, mask:Mask=null)
		{
			super(x, y, g, null);
		}
	}
}