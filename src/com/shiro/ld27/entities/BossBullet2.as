package com.shiro.ld27.entities
{
	import com.shiro.ld27.Assets;
	import com.shiro.ld27.Game;
	
	import net.flashpunk.FP;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Image;
	
	public class BossBullet2 extends Bullet
	{
		private var g:Image = new Image(Assets.BOSS_BULLET2);
		public function BossBullet2(x:Number=0, y:Number=0, direction:int=-1, graphics:Graphic=null)
		{
			super(x, y, direction, g);
			this.setHitbox(g.width,g.height);
			this.type = "enemyBullet";
		}
		override public function update():void
		{
			super.update();
			
			if(!Game.rewind)
			{
				this.y += speed * FP.elapsed;
				if(this.y > FP.screen.height + 100 || this.y < -100)
				{
					destroy(0);
				}
			}
		}
	}
}